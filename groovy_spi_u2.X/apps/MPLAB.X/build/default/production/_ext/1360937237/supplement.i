
# 1 "../src/supplement.c"

# 47 "../src/supplement.h"
char *my_itoa( int val, char *a, int radix );
unsigned char my_strlen(const char *aStr);

# 35 "../src/supplement.c"
char *my_itoa( int val, char *a, int radix )
{
char *p = a;
unsigned int v = val;
int n = 1;
while(v >= radix){
v /= radix;
n++;
}
p = a + n;
v = val;
*p = '\0';
do {
--p;
*p = v % radix + '0';
if(*p > '9') {
*p = v % radix - 10 + 'A';
}
v /= radix;
} while ( p != a);
return a;
}

unsigned char my_strlen(const char *aStr)
{
unsigned char length = 0;

while(*aStr++ != 0x00){
length++;
}
return length;
}


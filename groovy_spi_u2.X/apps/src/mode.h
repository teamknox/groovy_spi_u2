/* 
 * File:   mode.h
 * Author: ooo
 *
 * Created on February 15, 2022, 11:19 AM
 */

#ifndef MODE_H
#define	MODE_H

#ifdef	__cplusplus
extern "C" {
#endif

    #define MODE_USB 1
    #define MODE_UART 0
    bool gSwMode;


#ifdef	__cplusplus
}
#endif

#endif	/* MODE_H */

